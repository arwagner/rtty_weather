﻿# DWD Weather reports via RTTY

## RTTY Text reports

Weather reports are transmitted in HF frequencies as follows:

| Frequency   | Transm | Operation time    | Power |
|-------------|--------|-------------------|-------|
| 4583.0 kHz  | DDK2   | 00:00 - 24:00 UTC | 1kW   |
| 7646.0 kHz  | DDH7   | 00:00 - 24:00 UTC | 1kW   |
| 10100.8 kHz | DDK9   | 00:00 - 24:00 UTC | 10kW  |
|-------------|--------|-------------------|-------|
| 147.3 kHz   | DDH47  | 00:00 - 24:00 UTC | 20kW  |
| 11039.0 kHz | DDH9   | 00:00 - 24:00 UTC | 10kW  |
| 14467.3 kHz | DDH8   | 00:00 - 24:00 UTC | 1kW   |

See also [`Radio Broadcasts` at
DWD](https://www.dwd.de/EN/specialusers/shipping/broadcast_en/_node.html) for
the actual time tables and more details.

These weather reports can be received by any HF radio that features single side
band reception. This can be a classical world receiver hooked up to the PC's
soundcard or a software defined radio that is capable to receive the frequencies
mentiond (usually requires some additional hardware to access the low HF
frequencies). To decode the messages [fldigi](http://www.w1hkj.com/) available
on all major platforms can be used. See also [SYNOP and SHIP Weather
Reports](http://www.w1hkj.com/doku/doku.php?id=faq:synop_and_ship_reports)

## Radio and fldigi setup

Parameter DDH 47

- SSB: **LSB**  (also check _USB_ + Rv in `fldigi`)
- For SW: use external antenna
- fldigi:
  - RTTY
    - Rx:
        - AFC speed: normal
        - RX - unshift on space: NOT set
        - Decode (CWI suppression): Mark-Space
    - Tx (Sound Card FSK):
        - Carrier shift: 85
        - Custom shift: (not relevant)
        - Baud rate: 50
        - Bits per character: 5
        - Parity: none
        - Stop bits: 1.5
        - AutoCRLF 72
        - Unset: _CR-CR-LF_, _TX-unshift on space_, _Pseudo-FSK - right channel_

Parameter HF

- SSB: **LSB**
- fldigi:
  - RTTY
    - Rx:
        - AFC speed: normal
        - RX - unshift on space: NOT set
        - Decode (CWI suppression): Mark-Space
    - Tx (Sound Card FSK):
        - Carrier shift: Custom
        - Custom shift: 450
        - Baud rate: 50
        - Bits per character: 5
        - Parity: none
        - Stop bits: 1.5
        - AutoCRLF 72
        - Unset: _CR-CR-LF_, _TX-unshift on space_, _Pseudo-FSK - right channel_

To create a text mode log file use `File/Text capture` and set `Log all RX/TX
text`. This will create a text file `$HOME/.fldigi/fldigi<date>.log`

To extract a more compact report from these logs

```
cat fldigi20200326.log | grep -v RYRY | grep -v "CQ CQ " | grep -v "FREQUEN" | grep -v ": $" | cut -c 18- 
```

This will strip RX line numbers and protocol and drop out empty lines as well
the technical messages (`CQ`, `RY` and frequency announcements).

To see a running log make sure to set all tools to line buffering:

```
 tail -f fldigi20200326.log | grep --line-buffered -v RYRY | grep --line-buffered -v "CQ CQ " | grep --line-buffered -v "FREQUEN" | grep --line-buffered -v ": $" | stdbuf -oL cut -c18-
```

(`cut` needs `stdbuf -oL`; note that 18 ist the length for RTTY, NavTeX would be 17)


> Because RTTY code represent letters and numbers with shift code, it might
> happen that a transmission error unexpectedly shifts numbers to letters. It
> is possible to turn this behaviour off in the RTTY configuration by turning
> off the “Unshift on Space”, these spaces being the ones between group of
> words.  That way, it keeps the numbers displayed in all circumstances. 

Translation table:

  |   |   |   |   |   |   |   |   |   |  |   |   |   |   |   |   |   |   |  
--|---|---|---|---|---|---|---|---|---|--|---|---|---|---|---|---|---|---|-- 
Q | W | E | R | T | Y | U | I | O | P |  | + | : | = | ? | , | ' | # | ( | )
1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0 |  | Z | C | V | B | N | S | D | K | L

With proper settings this translation is not be necessary if reception is good.

## DWD Navtex

490 kHz - German version (Letter L):

- 01:50 - 02:00 Ostsee: Windwarnungen und nautische Warnungen, Wettervorhersagen
- 05:50 - 06:00 Nordsee: Windwarnungen und nautische Warnungen, Wettervorhersagen
- 09:50 - 10:00 Ostsee: Windwarnungen und nautische Warnungen, ggf. Eisberichte
- 13:50 - 14:00 Nordsee: Windwarnungen und nautische Warnungen, ggf. Eisberichte
- 17:50 - 18:00 Ostsee: Windwarnungen und nautische Warnungen, Wettervorhersagen
- 21:50 - 22:00 Nordsee: Windwarnungen und nautische Warnungen, Wettervorhersagen

518 kHz - English version (Letter S):

- 03:00 UTC
- 07:00 UTC
- 11:00 UTC
- 15:00 UTC
- 19:00 UTC 
- 23:00 UTC


### Message format fldigi text log

- Start of line:
  - `RX`
  - frequency (e.g. `151596` for 151.596 kHz)
  - ` : `
  - Message=Modem type (e.g. `RTTY`, `TOR` = NavTeX)
  - `(YYYY-MM-DD hh:mmZ)`: reception time in UT
  - `: ` start of content
- Start of message block: `ZCZC <number>`
- Service ID: next empty line after start signal
- End of message block: `NNNN`
- Within message: 
  - each message line followed by empty line (` <cr>`)
  - each sub-block separated by 2 empty lines

## Weather fax from DWD

- fldigi mode `WEFAX576`
- Configure/UI/Modems -> Other / WFx: set `Frequency shift` to `850`
- Disable `AFC`!
- `Slant`: 0.0090 (depends on sound card)
- `Align`: 0 seems good with enabled automatic centering
- `FIR`: Hanning seems to get best results
- `Noise`: enable
- Reception in _Hamburg_: seems 7880kHz (DDK3) or 13882kHz (DDK6) to be the best

Received images are stored in `.fldigi/images/wefax_<date>_<time>_<freq>_ok.png`
where `<freq>` is the frequency set in fldigi. Using a radio this does not have
any relevance.

Notes:

- With the default settings `fldigi` splits the images automatically per
reception.
- Automatic alignment works only if the full image is received.

It can help to set `Bin` (forces black/white) and adopt the level below to a
suitable value to filter out gray noise. The default `128` might be to low
(suppress to many dots).

Note on _AFC for fax_: It can theoretically be enabled if a larger number of
lines was decoded properly, but it usually just move the reception window to the
border of the waterfall instead of the signal. Especially, if a new image comes
in.

## Transforming reception to a simple static web page

To this end the supplied python script `rtty_weather.py` can be used. It requires
`fldigi` to write a log file of it's reception and parse this log ot html.

See

```
$ rtty_weather.py --help`
```

for all paramerters. A usual call would be

```
$ rtty_weather.py -o $HOME/.fldigi/RadioWeather.html --latest -f 600 -d 
```

This would take only the most recent log (`--latest`) into account and refresh
the web page after `-f` 600 seconds (aka 10min). Mind that RTTY transmits at 50
baud and a new message block is only added to the page once it was completely
received. `-d` finally will run the tool in an endless loop (daemon mode). It
will, however, not detach from the terminal automatically. The `-o` output is
written to `$HOME/.fldigi/RadioWeather.html` where it can be recalled by any web
browser.

Note that the HTML is pretty simple minded. Hence one has to palce the necessary
css files for styling in the same directory where the html file is generated.
The css used is called `dwdrtty.css`. The included file serves as a sample for
own customizations. By default it uses `solarized-dark.css` as a base for the
colours, as they are easy to the eye.

## Sample Output

![Sample output](screenshot.png)

# Weather reports from DWD OpenData

`od_weather.py` offers similar functionality as `rtty_weather.py` but fetches
data from the DWD web pages (charts) and the DWD OpenData server (textual
reports). For parameters see

```
$ od_weather.py --help
```

Note that some reports may be available in German only.

<!--
# UK Met Office

**Untested**

Transmission: GYA = Northwood Radio, UK

Schedule: 
- [RN Northwood](http://www.users.zetnet.co.uk/tempusfugit/marine/fwoc.htm)
- [Yachtcom](http://info.yachtcom.co.uk/Weather/)

Frequencies:

- 2618.5 (2616.6) kHz*
- 4610 (4608.1) kHz
- 8040 (8038.1) kHz
- **11086.5 (11084.6) kHz**
- 18261	(18259.1) kHz*

<i>in brackets() frequency to tune to using USB mode. * not 24 hours</i>

Transmitter is switched off 
-->


<!-- vim: spell spelllang=en_gb
-->

