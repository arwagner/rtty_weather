#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Invenio is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Invenio; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307, USA.

"""
Parse fldigi log files that were generated retrieving RTTY signals from DWD and
create an easily readable web page from them. Also allow for simple command
line selection of any given reports.
"""

# Variable naming conventions: we use Uppercase for function names:
# pylint: disable=C0103

import argparse
import logging
import sys
import os
sys.path.append(os.path.dirname(sys.executable))

logging.basicConfig()
logger = logging.getLogger(__name__)


def GetCurrentLogFile(path='%s/.fldigi' % os.environ['HOME'], latest=True):
    """
    Return the most recent log file form .fldigis dir

    @param path: path to the fldigi log files
    @param latest: only use latest log or all?
    """
    import glob
    import os

    logger.info('Identify log files')

    files = glob.glob('%s/fldigi*.log' % path)
    logger.debug('Files found: %s', files)
    if latest:
        logger.info('Evaluating only last file')
        return [max(files, key=os.path.getctime)]
    else:
        logger.info('Evaluating all files')
        return files


def ReadLog(filename):
    """
    Read the whole log and split it into a list of lines

    @param filename: file to read, this file is split linewise into a list
    """
    logger.debug('Read log file %s', filename)
    f = open(filename, 'rb')
    lines = f.read().split('\n')
    f.close()
    return lines


def SplitMessages(loglines):
    """
    Split the log into individual messages.

    @param loglines: fldigi log lines as list (one line per entry)
    """
    import re
    from dwd_weather import GetEmptyMessage, FormatTextLine

    logger.debug('Start parsing text file')

    messages = []

    startfound = False
    endfound = False
    # message global metadata
    message = GetEmptyMessage()
    lastcontent = ''

    for line in loglines:
        if not '): ' in line:
            continue
        header, content = line.split('): ')
        header = header.strip()
        content = content.strip()

        if 'ZCZC' in line:
            # start of message
            logger.debug('Start of message found')
            message = GetEmptyMessage()
            message['starttime'] = header.split(' (')[1]  # time in UT
            message['endtime'] = '0000-00-00 00:00Z'
            message['msgno'] = content.replace('ZCZC ', '').strip()
            logger.debug('Message number: %s', message['msgno'])
            startfound = True
            endfound = False
        elif ('EDZW' in line) or ('EFKL' in line):
            # transmission type
            logger.debug('Transmission type detected')
            dummy = content.split(' ')
            message['header'] = dummy[0]
            message['dtatime'] = dummy[2]
        elif 'NNNN' in line:
            # end of message
            logger.debug('End of message found')
            endfound = True
            startfound = False
            message['endtime'] = header.split(' (')[1]  # time in UT
            message['text'] = re.sub(
                '\n{3,}', '\n\n',
                '\n'.join(message['lines']).strip()) # .replace(':\n', ': ')
            message['lines'] = []
            messages.append(message)
        elif startfound and not endfound:
            if content != '':
                logger.debug('Adding content line %s', content)
                message['lines'].append(FormatTextLine(content))
            if (lastcontent == '') and (content == ''):
                logger.debug('Adding newline')
                message['lines'].append('\n')
            lastcontent = content

    return messages


def SelectByType(search, msgdict):
    """
    Select a message by type and return all keys that match

    @param type: header type
    @param msgdict: dictionary of messages
    """
    logger.debug('Selecting message by key %s', search)
    matches = []
    for key in msgdict:
        if search.upper() in key.upper():
            matches.append(key)
    return matches


def ListMessages(msgdict):
    """
    List all messages and their long name

    @param msgdict
    """
    logger.info('Extract list of messages')
    catalog = GetMsgTranslations()
    for key in msgdict:
        print "%s: %s" % (key, GetMessageTitle(catalog, key))
    return


#======================================================================
def main(logpath, latest, listmsg, getmessages, outfile, refresh):
    """
    """
    from dwd_weather import \
        Messages2Dict,      \
        ToHTML,             \
        WriteHTML

    msgs = []

    # build a page from all log files available.
    logger.info('Parsing files and retrieving messages')
    for filename in GetCurrentLogFile(path=logpath, latest=latest):
        msgs += SplitMessages(ReadLog(filename))

    logger.info('Converting messages to dict')
    msgdict = Messages2Dict(msgs)

    if listmsg:
        logger.info('Give list of messages only')
        ListMessages(msgdict)
        return

    if getmessages != '':
        logger.info('Return messages as selected: %s', getmessages)
        fqmsgs = []
        for selectkey in getmessages:
            fqmsgs += SelectByType(selectkey, msgdict)
        for key in fqmsgs:
            print ''
            print '---------- [%s] ----------' % key
            print msgdict[key]['text']
        return

    titleappend = ''
    if latest:
        titleappend = '(only latest log)'
    logger.info('Creating HTML output for browsers')
    WriteHTML(
        ToHTML(msgdict),
        title='Radio Weather Broadcasts %s' % titleappend,
        refresh=refresh,
        outfile=outfile)

    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', default = 0, action='count')
    parser.add_argument('--status', '-s', default=0, action='count',
                        help="Return the list of logs available for building "
                             "the weather report.")
    parser.add_argument('--path', '-p', default='%s/.fldigi' % os.environ['HOME'],
                        help="Path, where fldigi creates its text log files. "
                             "(default: %s/.fldigi)" % os.environ['HOME'])
    parser.add_argument('--outfile', '-o', default='RadioWeather.html',
                        help="HTML output to generate "
                             "(default: RadioWeather.html)")
    parser.add_argument('--all', '-a', default=False,
                        dest='latest', action='store_false',
                        help="Use all available log files. This will include "
                             "older weather reports in the output an thus "
                             "increase the the file and its sections. "
                             "However, it may include older reports that "
                             "are still current because they were not yet "
                             "updated. (default).")
    parser.add_argument('--latest', default=False,
                        dest='latest', action='store_true',
                        help="Evaluate only them most recent log file.")
    parser.add_argument('--daemon', '-d', default=False,
                        dest='daemon', action='store_true',
                        help="Run as a daemon. The program will enter a "
                             "loop and runn till it is killed")
    parser.add_argument('--frequency', '-f', default='300',
                        help="Frequency in seconds for data refresh."
                             "(default: 300s = 5min")
    parser.add_argument('--list-messages', '-l', default=False,
                        dest='listmsgs', action='store_true',
                        help="Give a list of all messages available without "
                             "acutally printing them.")
    parser.add_argument('--get-messages', '-g', nargs='+', default='',
                        dest='getmsgs',
                        help="Retrieve messages by key and print them to "
                             "stdout.")

    args = parser.parse_args()

    if args.verbose == 0:
        # Default
        log_level = logging.WARNING
    elif args.verbose == 1:
        log_level = logging.INFO
    elif args.verbose == 2:
        log_level = logging.DEBUG
    else:
        log_level = logging.ERROR

    print ('Parse fldigi log files for weather reports received via RTTY')
    print ('Parameters:')
    print ('--status       : %s' % str(args.status))
    print ('--path         : %s' % str(args.path))
    print ('--all          : %s' % str(args.latest))
    print ('--list-messages: %s' % str(args.listmsgs))
    print ('--get-messages : %s' % str(args.getmsgs))
    print ('--outfile      : %s' % str(args.outfile))
    print ('--daemon       : %s' % str(args.daemon))
    print ('--frequency    : %s' % str(args.frequency))

    if args.status == 1:
        logger.info('Printing only status as instructed')
        for filename in GetCurrentLogFile(
            path=args.path, latest=args.latest):
                print filename
        exit
    logger.setLevel(log_level)
    if args.daemon:
        import time
        done = 0
        while(True):
            print('Refreshes done: %i' % done)
            done += 1
            main(
                logpath=args.path,
                latest=args.latest,
                listmsg=args.listmsgs,
                getmessages=args.getmsgs,
                refresh=args.frequency,
                outfile=args.outfile
                )
            time.sleep(float(args.frequency))
    else:
        main(
            logpath=args.path,
            latest=args.latest,
            listmsg=args.listmsgs,
            getmessages=args.getmsgs,
            refresh=args.frequency,
            outfile=args.outfile
            )
