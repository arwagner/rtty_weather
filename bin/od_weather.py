#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Alexander Wagner
#
# Invenio is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# Invenio is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Invenio; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307, USA.

"""
DWD OpenData sea weather report formatter

Format downloaded raw data to an easily readable HTML file.
"""

# Variable naming conventions: we use Uppercase for function names:
# pylint: disable=C0103

import argparse
import logging
import sys
import os
sys.path.append(os.path.dirname(sys.executable))
logging.basicConfig()
logger = logging.getLogger(__name__)


def FetchReports(ln='en', path='./RAW'):
    """
    Fetch textual reports from DWD OpenData

    @param ln: `de` or `en`
    @param path: where to store the files
    """
    import os
    import urllib.request, urllib.error, urllib.parse

    logger.info('Fetching textual reports from DWD OpenData')

    try:
        os.makedirs(path)
    except:
        logger.warning("Can't create %s, don't fetch files'", path)
        return

    txtprefix = 'https://opendata.dwd.de/weather/maritime/forecast/'
    reports = []
    if ln == 'de':
        txtprefix += 'german'
        reports = [
            'FQEN50_EDZW_LATEST',
            'FQEN51_EDZW_LATEST',
            'FXDL40_EDZW_LATEST',
            'FQMM60_EDZW_LATEST',
            'WODL45_EDZW_LATEST',
            'WODL45_EDZW_LATEST_COR' ,
        ]
    else:
        txtprefix += 'english'
        reports = [
            'FQEN70_EDZW_LATEST',
            'FQEN71_EDZW_LATEST',
            'FQMM80_EDZW_LATEST',
            'WODL45_EDZW_LATEST',
            'WODL45_EDZW_LATEST_COR',
        ]

    addprefix = 'https://opendata.dwd.de/weather/text_forecasts/txt/'
    additional = [
        'SXDL31_DWAV_LATEST',
        'SXDL33_DWAV_LATEST'
    ]

    for report in reports:
        url = '%s/%s' % (txtprefix, report)
        logger.info('Fetching: %s', url)
        try:
            response = urllib.request.urlopen(url)
            txt = response.read()
        except urllib.error.HTTPError:
            logger.warning('%s not found', report)
            continue

        f = open('%s/%s' % (path, report), 'wb')
        f.write(txt)
        f.close()

    for report in additional:
        url = '%s/%s' % (addprefix, report)
        logger.info('Fetching: %s', url)
        try:
            response = urllib.request.urlopen(url)
            txt = response.read()
        except urllib.error.HTTPError:
            logger.warning('%s not found', report)
            continue

        f = open('%s/%s' % (path, report), 'wb')
        f.write(txt)
        f.close()
    return


def FetchCharts(include=[]):
    """
    Fetch useful charts. It always includes the current synoptic chart.

    TODO
    Note: for simplicity, the sources are the general DWD web sites not the
          OpenData server

    @param include: list of special charts ot include from the following list:
        'northsea': North Sea +00, +24, +48, +72
        'balticsea': Baltic Sea +00, +24, +48, +72
        'eastatlantic': Eastern Atlantic +00, +24, +48, +72
        'eastmedi': Mediterranean Sea, Eastern part +00, +24, +48, +72
        'westmedi': Mediterranean Sea, Western part +00, +24, +48, +72


    TODO Potentially interesting charts from OpenData:

    Analyis charts (first line in colour second in b/w)
    https://opendata.dwd.de/weather/charts/analysis/
        Z__C_EDZW_LATEST_tka01%2Cana_bwkman_dwdna_O_000000_000000_LATEST_WV12.png
        Z__C_EDZW_LATEST_tka01%2Cana_bwkman_dwdna_O_000000_000000_202105220600_WV12SW.png_LATEST_WV12SW.png

    Germany:
    Two plots in one +6 and +12, +18 and +24, +30 and +36, +42 and +48
    - https://opendata.dwd.de/weather/charts/forecasts/icon/eu_nest/ce/

      * Significant weather forecast
      Z__C_EDZW_LATEST_nwv01%2Cicoeu_4g_ce_N_006012_999999_LATEST_WV11.png
      Z__C_EDZW_LATEST_nwv01%2Cicoeu_4g_ce_N_018024_999999_LATEST_WV11.png
      Z__C_EDZW_LATEST_nwv01%2Cicoeu_4g_ce_N_030036_999999_LATEST_WV11.png
      Z__C_EDZW_LATEST_nwv01%2Cicoeu_4g_ce_N_042048_999999_LATEST_WV11.png

      * Wind 10m forecast
      Z__C_EDZW_LATEST_nwv01%2Cicoeu_win_ce_N_006012_000010_LATEST_WV11.png
      Z__C_EDZW_LATEST_nwv01%2Cicoeu_win_ce_N_018024_000010_LATEST_WV11.png
      Z__C_EDZW_LATEST_nwv01%2Cicoeu_win_ce_N_030036_000010_LATEST_WV11.png
      Z__C_EDZW_LATEST_nwv01%2Cicoeu_win_ce_N_042048_000010_LATEST_WV11.png


    Europe:
    Two plots in one +6 and +12, +18 and +24, +30 and +36, +42 and +48
    - https://opendata.dwd.de/weather/charts/forecasts/icon/eu_nest/cex/

        * Significant weather forecast
        Z__C_EDZW_LATEST_nwv01%2Cicoeu_4g_cex_N_006012_999999_LATEST_WV11.png
        Z__C_EDZW_LATEST_nwv01%2Cicoeu_4g_cex_N_018024_999999_LATEST_WV11.png
        Z__C_EDZW_LATEST_nwv01%2Cicoeu_4g_cex_N_030036_999999_LATEST_WV11.png
        Z__C_EDZW_LATEST_nwv01%2Cicoeu_4g_cex_N_042048_999999_LATEST_WV11.png

        * Wind 10m forecast
        Z__C_EDZW_LATEST_nwv01%2Cicoeu_win_cex_N_006012_000010_LATEST_WV11.png
        Z__C_EDZW_LATEST_nwv01%2Cicoeu_win_cex_N_018024_000010_LATEST_WV11.png
        Z__C_EDZW_LATEST_nwv01%2Cicoeu_win_cex_N_030036_000010_LATEST_WV11.png
        Z__C_EDZW_LATEST_nwv01%2Cicoeu_win_cex_N_042048_000010_LATEST_WV11.png

    """
    import datetime
    import urllib.request, urllib.error, urllib.parse
    from dwd_weather import GetEmptyMessage


    logger.info('Fetching specialized charts from DWD')

# https://opendata.dwd.de/weather/charts/analysis/Z__C_EDZW_LATEST_tka01%2Cana_bwkman_dwdna_O_000000_000000_202105220600_WV12SW.png_LATEST_WV12SW.png
# https://opendata.dwd.de/weather/charts/analysis/Z__C_EDZW_LATEST_tka01%2Cana_bwkman_dwdna_O_000000_000000_LATEST_WV12.png
    prefix = 'https://www.dwd.de/DWD/wetter/wv_spez'

    charts = {}

    othercharts = {
        'synoptic': {
            'bwk_bodendruck_na_ana': 'hobbymet/wetterkarten/bwk_bodendruck_na_ana.png',
        },
        'northsea': {
            'nordsa_00': 'seewetter/nordsa_00.png',
            'nordsa_24': 'seewetter/nordsa_24.png',
            'nordsa_48': 'seewetter/nordsa_48.png',
            'nordsa_72': 'seewetter/nordsa_72.png',
        },

        'balticsea': {
            'ostsa_00': 'seewetter/ostsa_00.png',
            'ostsa_24': 'seewetter/ostsa_24.png',
            'ostsa_48': 'seewetter/ostsa_48.png',
            'ostsa_72': 'seewetter/ostsa_72.png',
        },

        'eastatlantic': {
            'oantik_00': 'seewetter/oantik_00.png',
            'oantik_24': 'seewetter/oantik_24.png',
            'oantik_48': 'seewetter/oantik_48.png',
            'oantik_72': 'seewetter/oantik_72.png',
        },

        'westmedi': {
            'wmitme_00': 'seewetter/wmitme_00.png',
            'wmitme_24': 'seewetter/wmitme_24.png',
            'wmitme_48': 'seewetter/wmitme_48.png',
            'wmitme_72': 'seewetter/wmitme_72.png',
        },

        'eastmedi': {
            'omitme_00': 'seewetter/omitme_00.png',
            'omitme_24': 'seewetter/omitme_24.png',
            'omitme_48': 'seewetter/omitme_48.png',
            'omitme_72': 'seewetter/omitme_72.png',
        }
    }

    for incl in include:
        charts.update(othercharts[incl])

    messages = []

    for chart in charts:
        url = '%s/%s' % (prefix, charts[chart])
        logger.info('Fetching: %s', url)

        try:
            response = urllib.request.urlopen(url)
        except urllib.error.HTTPError:
            logger.warning('Could not fetch %s', url)
            continue

        png = response.read()
        fn = charts[chart].split('/')[-1]
        # TODO we use the current dir here
        f = open(fn, 'wb')
        f.write(png)
        f.close()

        # Create a suitable message so the chart get's included in the final
        # html
        message = GetEmptyMessage()
        message['header'] = chart
        message['dtatime'] = datetime.datetime.now().strftime('%d%H%M')
        time = '%s:%s' % (message['dtatime'][2:4], message['dtatime'][4:6])
        message['starttime'] = '%s-%s %sZ' % (
            datetime.datetime.now().strftime('%Y-%m'),
            message['dtatime'][:2], time
        )
        message['endtime'] = message['starttime']

        style = [
            "max-width: 100%",
            "height:auto",
            "margin: 5px",
            "border: 1px solid black",
        ]
 
        message['text'] = '<img style="%s" src="%s">' % (
            '; '.join(style), fn)
        messages.append(message)
    return messages


def ReadMessage(fn, report):
    """
    Read a message from the given file to a message dict

    @param fn: filename to read
    @param report: code for report type
    """
    import codecs
    import datetime
    import re
    from dwd_weather import GetEmptyMessage, FormatTextLine

    logger.info('Parsing %s from %s', report, fn)
    # Reports are encoded as latin-1 => recode to utf-8
    f = codecs.open(fn, 'r', 'latin-1')
    content = f.readlines()
    f.close()

    message = GetEmptyMessage()
    dummy = fn.split('_')
    message['msgno'] = dummy[-1].replace('LATEST', '999')
    if report.upper() == 'SXDL33':
        # this report does not have a header :S - Fake it to get a display
        message['header'] = report
        message['dtatime'] = datetime.datetime.now().strftime('%d%H%M')
        time = '%s:%s' % (message['dtatime'][2:4], message['dtatime'][4:6])
        message['starttime'] = '%s-%s %sZ' % (
            datetime.datetime.now().strftime('%Y-%m'),
            message['dtatime'][:2], time
        )
        message['endtime'] = message['starttime']

    for line in content:
        line = line.replace('', '')
        line = line.replace('\r', '')
        line = line.replace('', '')
        line = line.strip()
        if ('DWHA' in line) or ('DWAV' in line):
            # parse the header
            logger.debug('Parse header: %s', line)
            dummy = line.split(' ')
            message['header'] = dummy[0]
            message['dtatime'] = dummy[2]

            time = '%s:%s' % (message['dtatime'][2:4], message['dtatime'][4:6])
            message['starttime'] = '%s-%s %sZ' % (
                datetime.datetime.now().strftime('%Y-%m'),
                message['dtatime'][:2], time
            )
            message['endtime'] = message['starttime']
            message['header'] = report  # OD data changes the name sometimes
        else:
            line = FormatTextLine(line)
            if line:
                message['lines'].append(line)

    message['text'] = re.sub(
        '\n{2,}', '\n',
        '\n'.join(message['lines']).strip()) # .replace(':\n', ': ')

    return message


def MessagesFromFiles(oddownload='./RAW'):
    """
    Read in all files in dir and try to extract weather messages from DWD

    @param oddownload: path to files downloaded from DWD OpenData
    """
    import os
    from dwd_weather import GetMsgTranslations

    logger.info('Parsing files and retrieving messages from %s', oddownload)

    files = os.listdir(oddownload)
    knownreports = GetMsgTranslations()
    messages = []
    for fn in files:
        known = False
        for rep in knownreports:
            if fn.startswith(rep):
                logger.info('Report found: %s = %s',
                             fn, knownreports[rep])
                known = True
                messages.append(ReadMessage('%s/%s' % (oddownload, fn), rep))
    logger.info('%i messages found', len(messages))
    return messages


#======================================================================
def main(ln='en', raw="./RAW", outfile='ODWeather.html', charts=[]):
    """
    """
    from dwd_weather import WriteHTML, ToHTML, Messages2Dict
    logger.info("Starting to fetch reports")

    FetchReports(ln=ln, path=raw)
    messages = MessagesFromFiles()
    messages += FetchCharts(include=charts)
    msgdict = Messages2Dict(messages)
    WriteHTML(
        ToHTML(msgdict),
        title='OpenData Weather Download',
        refresh=0, outfile=outfile
    )

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', default = 0, action='count')
    parser.add_argument(
        '--outfile', '-o', default='ODWeather.html',
        help="HTML output to generate"
             " (default: ODWeather.html)")
    parser.add_argument(
        '--language', '-l', default='en',
        choices=['de', 'en'],
        help="Preferred language for textual reports, en or de"
             " (Default: en)"
        )
    parser.add_argument(
        '--raw', '-r', default='./RAW',
        help="Where to store the raw download data."
             " (Default: `./RAW`)"
        )
    parser.add_argument(
        '--charts', '-c', nargs='+', default=[],
        dest='charts',
        choices=['synoptic', 'northsea', 'balticsea', 'eastatlantic',
                 'eastmedi', 'westmedi'],
        help="Which charts should be downloaded? Sea areas will fetch charts"
             " +00h, +24h, +48h, +72h"
        )

    args = parser.parse_args()
    if args.verbose == 0:
        # Default
        log_level = logging.WARNING
    elif args.verbose == 1:
        log_level = logging.INFO
    elif args.verbose == 2:
        log_level = logging.DEBUG
    else:
        log_level = logging.ERROR
    logger.setLevel(log_level)
    main(raw=args.raw,
         outfile=args.outfile,
         charts=args.charts,
         ln=args.language)

