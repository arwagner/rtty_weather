#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Alexander Wagner
#
# Invenio is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# Invenio is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Invenio; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307, USA.

"""
dwd_weather: common routines to handle DWD weather reports for reception via
RTTY (using fldigi log files) or direct download from DWD OpenData
"""
import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

# Variable naming conventions: we use Uppercase for function names:
# pylint: disable=C0103

def GetMsgTranslations():
    """
    Return translations of codes to (official) names

    TODO should this be an input file?
    """
    type2name = {
        # 1. Program
        'FQEN70': 'Seewetterbericht Nord- und Ostsee',
        'FQEN71': 'Küstenwetterbericht Nord- und Ostsee',
        'FEBQ72': 'Zeitreihenbericht Mittelfrist Ostsee, Prognose 5 Tage',
        'FEEN73': 'Zeitreihenbericht Mittelfrist Nordsee, Prognose 5 Tage',
        'FEMM74': 'Zeitreihenbericht Mittelfrist Mittelmeer, Prognosen für 5 Tage',
        'FQEN75': 'Zeitreihenbericht Norwegische See & Ostsee',
        'FAEA75': 'Zeitreihenbericht Mittelfrist Ostatlantik, Prognosen für 5 Tage',
        'FQNT76': 'Zeitreihenbericht Nordatlantik (Pentlands bis Südwestgrönland), Prognosen für 2 Tage',
        'FQEQ77': 'Zeitreihenbericht westeurop. Gewässer (südl. Irland bis Kanaren Süd), Prognosen für 2 Tage',
        'FQMM78': 'Zeitreihenbericht westliches Mittelmeer (Alboran bis Tunis), Prognosen für 2 Tage',
        'FQMM79': 'Zeitreihenbericht östliches Mittelmeer (östlich Tunis bis Rhodos/Zypern), Prognosen für 2 Tag',
        'FQMM80': 'Seewetterbericht Mittelmeer in Textform',
        'SMVX41-45': 'Schiffswettermeldungen aus den Seegebieten Nordatlantik und EG-Meer ',
        'SIVX41-45': 'Schiffswettermeldungen aus den Seegebieten Nordatlantik und EG-Meer ',
        'SMVX25-27': 'Bojenwettermeldungen aus den Seegebieten Atlantik und Nordpolarmeer',
        'NOXX70': 'Hinweis auf Datennutzung',
        'NODL61': 'Hinweise zu Störungen und Ausfällen',


        # 2. Program
        'FQEN50': 'Seewetterbericht Nord- und Ostsee',
        'FQEN51': 'Küstenwetterbericht Nord- und Ostsee',
        'FEBQ52': 'Zeitreihenbericht Mittelfrist Ostsee, Prognosen für 5 Tage',
        'FEEN53': 'Zeitreihenbericht Mittelfrist Nordsee, Prognosen für 5 Tage',
        'FEMM54': 'Zeitreihenbericht Mittelfrist Mittelmeer, Prognosen für 5 Tage',
        'FEAE55': 'Zeitreihenbericht Mittelfrist Ostatlantik, Prognosen für 5 Tage',
        'FQEN55': 'Zeitreihenbericht Norwegische See & Ostsee',
        'FQNT56': 'Zeitreihenbericht Nordatlantik (Pentlands bis Südwestgrönland), Prognosen für 2 Tage',
        'FQEW57': 'Zeitreihenbericht westeurop. Gewässer (südl. Irland bis Kanaren Süd), Prognosen für 2 Tage',
        'FQMM58': 'Zeitreihenbericht westliches Mittelmeer (Alboran bis Tunis), Prognosen für 2 Tage',
        'FQMM59': 'Zeitreihenbericht östliches Mittelmeer (östlich Tunis bis Rhodos/Zypern), Prognosen für 2 Tag',
        'FQMM60': 'Seewetterbericht Mittelmeer in Textform',
        'SXEN40': 'Stationsmeldungen Nordsee / Ostsee',
        'SXMM41': 'Stationsmeldungen Mittelmeer',
        'SMVX41': 'Schiffswettermeldungen aus den Seegebieten Nordatlantik und EG-Meer ',
        'NOXX50': 'Hinweis auf Datennutzung',
        'NODL40': 'Hinweise zu Störungen und Ausfällen',

        # --------------------------------------------------------------------
        # All
        'WODL45': 'Sturmwarnungen für Deutsche Bucht, Westliche und Südliche Ostsee sowie Nord- und Ostseeküste',
        'WOBQ61': 'Warnungen für die Ostsee (englisch)',
        'WOEN69': 'Warnungen für die Nord- und Ostsee (englisch)',
        'WWXX60': 'Nautische Warnnachrichten',
        'WOEN88': 'Warnungen: Wetter',

        # ---- 1. program, shorter labels ----
        'FQEN70': 'Seewetter Nord- und Ostsee',
        'FQEN71': 'Küstenwetter Nord- und Ostsee',
        'FEBQ72': 'Mittelfrist Ostsee, 5d',
        'FEEN73': 'Mittelfrist Nordsee, 5d',
        'FEMM74': 'Mittelfrist Mittelmeer, für 5d',
        'FQEN75': 'Mittelfrist Norwegische See & Ostsee',
        'FAEA75': 'Mittelfrist E-Atlantik, 5d',
        'FQNT76': 'Mittelfrist N-Atlantik (Pentlands bis Südwestgrönland), 2d',
        'FQEQ77': 'Mittelfrist W-europ. Gewässer (S-Irland bis Kanaren S), 2d',
        'FQMM78': 'Mittelfrist W-Mittelmeer (Alboran bis Tunis), 2d',
        'FQMM79': 'Mittelfrist E-Mittelmeer (E Tunis bis Rhodos/Zypern), 2d',
        'FQMM80': 'Seewetter Mittelmeer',
        'SMVX41-45': 'Schiffswettermeldungen: Nordatlantik und EG-Meer ',
        'SIVX41-45': 'Schiffswettermeldungen: Nordatlantik und EG-Meer ',
        'SMVX25-27': 'Bojenwettermeldungen: Atlantik und Nordpolarmeer',
        'NOXX70': 'Hinweis auf Datennutzung',
        'NODL61': 'Hinweise zu Störungen und Ausfällen',
        # ---- 2. program, shorter ----
        'FQEN50': 'Seewetter Nord- und Ostsee',
        'FQEN51': 'Küstenwetter Nord- und Ostsee',
        'FEBQ52': 'Mittelfrist Ostsee, Prognosen 5d',
        'FEEN53': 'Mittelfrist Nordsee, Prognosen 5d',
        'FEMM54': 'Mittelfrist Mittelmeer, Prognosen 5d',
        'FEAE55': 'Mittelfrist E-Atlantik, Prognosen 5d',
        'FQEN55': 'Mittelfrist Norwegische See & Ostsee',
        'FQNT56': 'Mittelfrist N-Atlantik (Pentlands bis SW-Grönland), Prognosen 2d',
        'FQEW57': 'Mittelfrist W-europ. Gewässer (S-Irland bis Kanaren S), Prognosen 2d',
        'FQMM58': 'Mittelfrist W-Mittelmeer (Alboran bis Tunis), Prognosen 2d',
        'FQMM59': 'Mittelfrist E-Mittelmeer (E Tunis bis Rhodos/Zypern), Prognosen 2d',
        'FQMM60': 'Seewetter Mittelmeer',
        'SXEN40': 'Stationsmeldungen Nordsee / Ostsee',
        'SXMM41': 'Stationsmeldungen Mittelmeer',
        'SMVX41': 'Schiffswettermeldungen: Nordatlantik und EG-Meer ',
        'NOXX50': 'Hinweis auf Datennutzung',
        'NODL40': 'Hinweise zu Störungen und Ausfällen',
        # ---- all, shorter labels ----
        'WODL45': 'Warnungen: Starkwind & Sturm',
        'WOBQ61': 'Warnungen: Ostsee',
        'WOEN69': 'Warnungen: Nord- und Ostsee',
        'WOEN88': 'Warnungen: Wetter',
        'WWXX60': 'Nautische Warnnachrichten',

        # Other
        'STFI42': 'Finnish Ice Report',

        # OpenData reports
        'FXDL40': 'Mittelfrist Nord- und Ostsee',
        'SXDL31': 'Kurzfrist Deutschland',
        'SXDL33': 'Mittelfrist Deutschland',
        'SXEU31': 'Kurzfrist Deutschland',
        'SXEU33': 'Mittelfrist Deutschland',

        # Charts from OpenData / regular web pages
        'bwk_bodendruck_na_ana': 'Chart: Synopsis',

        'nordsa_00': 'Chart Nordsee +00h',
        'nordsa_24': 'Chart Nordsee +24',
        'nordsa_48': 'Chart Nordsee +48',
        'nordsa_72': 'Chart Nordsee +72',

        'ostsa_00': 'Chart Baltic Sea +00h',
        'ostsa_24': 'Chart Baltic Sea +24h',
        'ostsa_48': 'Chart Baltic Sea +48h',
        'ostsa_72': 'Chart Baltic Sea +72h',

        'oantik_00': 'Chart East Atlantic +00h',
        'oantik_24': 'Chart East Atlantic +24h',
        'oantik_48': 'Chart East Atlantic +48h',
        'oantik_72': 'Chart East Atlantic +72h',

        'wmitme_00': 'Chart Western Mediterranean +00h',
        'wmitme_24': 'Chart Western Mediterranean +24h',
        'wmitme_48': 'Chart Western Mediterranean +48h',
        'wmitme_72': 'Chart Western Mediterranean +72h',

        'omitme_00': 'Chart Eastern Mediterranean +00h',
        'omitme_24': 'Chart Eastern Mediterranean +24h',
        'omitme_48': 'Chart Eastern Mediterranean +48h',
        'omitme_72': 'Chart Eastern Mediterranean +72h',
    }
    return type2name


def getMsgPriority():
    """
    Return a list of lists with the message priorities so they sort the same
    way nicely all the time and the sorting reflects on priorities
    """
    msgpriority = [
        # Warnings
        ['WODL45', 'WWXX60', 'WOEN69', 'WOEN88', 'WOBQ61', 'NODL40', 'NODL61'],
        # Coastal weather
        ['FQEN51', 'FQEN71'],
        # Sea weather
        [
            'FQEN70', 'FQEN50',
            'FQMM80', 'FQMM60',
            'STFI42',
        ],
        ['FXDL40'],
        ['SXEU31', 'SXDL31', 'SXEU33', 'SXDL33'],
        ['bwk_bodendruck_na_ana',
         'nordsa_00', 'nordsa_24', 'nordsa_48', 'nordsa_72',
         'ostsa_00', 'ostsa_24', 'ostsa_48', 'ostsa_72',
         'oantik_00', 'oantik_24', 'oantik_48', 'oantik_72',
         'wmitme_00', 'wmitme_24', 'wmitme_48', 'wmitme_72',
         'omitme_00', 'omitme_24', 'omitme_48', 'omitme_72',
         ],
        # Station reports
        [ 'SXEN40', 'SXMM41', 'SMVX41', 'SMVX41-45', 'SIVX41-45', 'SMVX25-27' ],
        # Timelines
        [
            'FEBQ72', 'FEBQ52',
            'FEEN73', 'FEEN53',
            'FEMM74', 'FEMM54',
            'FQEN75', 'FEAE55',
            'FAEA75', 'FQEN55',
            'FQNT76', 'FQNT56',
            'FQEQ77', 'FQEW57',
            'FQMM78', 'FQMM58',
            'FQMM79', 'FQMM59',
         ]
    ]
    logger.debug('Message priority: %s', msgpriority)
    return msgpriority


def GetMessageTitle(catalog, msgkey):
    """
    Return the long name of a message key.

    @param catalog: translations form GetMessageTitle
    @param msgkey: key to tanslate, e.g. WWXX60-270900
    """
    typekey, update = msgkey.split('-')
    if typekey in catalog:
        return catalog[typekey]
    else:
        return "No translation available!"


def FormatTextLine(line):
    """
    Text messages are unstructured in an IT sense, but contain some "markup" in
    natural language. Try to fetch this and replace it by HTML

    @param line: a line of text form DWD reports
    """

    # Declare terms that should be used to detect headings of vaiours levels
    headings = {
        'h3': [
            'mit Sturm zu rechnen',
            'WARNING',
            'mit Starkwind zu rechnen',
            'Synoptische Entwicklung bis',
            'Modellvergleich und -einsch',
            'Vorhersage bis',
            'Aussichten gueltig bis',
            'Vorhersage gueltig bis',

            'general synoptic situation',
            'strong winds',
            'forecast valid until',
            'are expected'
        ],
        'h4' : [
            'Nordseekueste:',
            'Vorhersage für die',
            'Ostseekueste:',
            '... ',
        ],
        'h5' : [
            ' :',
        ]
    }

    todo = True

    for heading in ['h3', 'h4', 'h5']:
        for term in headings[heading]:
            if (term in line) and todo:
                line = '<%s>%s</%s>' % (heading, line, heading)
                todo = False

    # special markers:
    if todo and line.startswith('NR.'):
        line = '<h3>%s</h3>' % line
        todo = False
    if todo and line.endswith(':'):
        line = '<h5>%s</h5>' % line
        todo = False
    if ('-----' in line) or ('_____' in line):
        line = '<hr/>'

    # in text highlighting
    makebold = [
        'Amtliche STARKWIND-Warnung',
        'Amtliche Aufhebung',
        'Gewitterboeen',
        'Gewitter',
        'schwere Schauerboeen',
        'orkanartige Boeen',
        'Schauerboeen',
        'Sturmtief',
        'Trog',
        'diesig',
        'neblig',
        'starker Regen',
        'Auslaeufer',

        'trough',
        'thundery gusts',
        'thundery',
        'misty',
    ]
    for word in makebold:
        line = line.replace(word, '<span class="warning">%s</span>' % word)
    for word in makebold:
        # RTTY is all upper case
        line = line.replace(word.upper(),
                            '<span class="warning">%s</span>' % word.upper)

    #coastalareas = [
    #    Ostfriesische Kueste:
    #    Elbmuendung:
    #    Seegebiet Helgoland:
    #    Nordfriesische Kueste:
    #    Elbe von Hamburg bis Cuxhaven:
    #]

    return line


def GetEmptyMessage():
    """
    Return an empty message dict. This structure is used later on troughout the
    modules. Keys:

    - starttime: start time of message reception
    - enddtime: end time of message reception
    - header: message name according to DWD
    - dtatime: coded date/time of the reprot, e.g. 20600: 20th 06:00 UTC
    - msgno: number of the message
    - text: the finally displayed text
    - lines: the lines of the message (not used for output)
    """
    logger.debug('New message requested')
    message = {}
    message['starttime'] = '0000-00-00 00:00Z'
    message['endtime'] = '0000-00-00 00:00Z'
    message['header'] = ''
    message['dtatime'] = '0000'
    message['msgno'] = '000'
    message['text'] = ''
    message['lines'] = []

    return message


def Messages2Dict(messages):
    """
    Convert a list of messages to a dictionary keyed by header and time for
    easy selection

    @param messages: list of messages each in `GetEmptyMessage()` format
    """
    logger.debug('Converting list of messages to dict')
    msgdict = {}
    for msg in messages:
        key = '%s-%s' % (msg['header'], msg['dtatime'])
        if key == '-0000':
            # Empty message generated
            continue
        logger.debug('Key %s generated', key)
        msgdict[key] = msg
    return msgdict


def ToHTML(msgdict):
    """
    Convert the given message dictionary to html

    @param msgdict: dictionary of messages
    """
    from dwd_weather import GetMsgTranslations
    type2name = GetMsgTranslations()

    logger.info('Sorting messages by priority')
    # Order elements in a priorized list of lists
    msgpriority = getMsgPriority()

    # message types in dict
    # Sort reverse so most current messages will float to the top in output
    thismsgtypes = sorted(list(msgdict.keys()), reverse=True)
    logger.debug('Available message types: %s', thismsgtypes)

    # sorteddict holds the keys retrieved in msgpriority order
    # Note that the msgdict keys also hold the time code in their keys!
    sorteddict = []
    done = []
    for prio in msgpriority:
        for msgtype in prio:
            for key in thismsgtypes:
                if key.startswith(msgtype):
                    sorteddict.append(key)
                    done.append(key)
    logger.debug('Messages sorted: %s', sorteddict)
    for key in thismsgtypes:
        if key not in done:
            logger.warning('Message key %s not yet known!', key)
            sorteddict.append(key)

    # remember message types once seen
    alreadyseen = []
    entries = []
    startul = False
    for key in sorteddict:
        cssclass = 'inactive'
        typekey, update = key.split('-')

        # Check if such a message was already there
        activemsg = True
        if typekey in alreadyseen:
            activemsg = False
        alreadyseen.append(typekey)

        if typekey in type2name:
            longtit = type2name[typekey]
        else:
            longtit = '%s (unknown message)' % typekey
        title = '[%(no)s] %(title)s: %(update)s (%(start)s-%(end)s)' % \
            {'title': typekey,
            'update': update,
            'start': msgdict[key]['starttime'],
            'end': msgdict[key]['endtime'],
            'no': msgdict[key]['msgno']
            }
        if activemsg:
            if startul:
                # suppress the first closing details as this would be wrong
                entries.append('</details>')
            startul = True
            longtit = '<span class="tiny">%s - </span>%s' % \
                (msgdict[key]['starttime'], longtit)
            title = '*** %s ***' % title
            cssclass = 'active'
            if typekey.startswith('W') or typekey.startswith('N'):
                headclass = 'warning'
            else:
                headclass = 'report'
            entries.append('<details class="%s">' % headclass)
            entries.append('<summary><h2 class="%s">%s</h2></summary>' %
                (headclass, longtit))
            entries.append('    <h3 class=%(css)s>%(tit)s</h3>' % {
                'css': cssclass,
                'tit': title} )
            entries.append('    <p class="%(css)s">%(txt)s</p>' % {
                'css': cssclass,
                'txt': msgdict[key]['text'].replace('\n', '<br/>')})
        else:
            entries.append('<details>')
            entries.append('<summary><h3 class=%(css)s>%(tit)s</h3></summary>' % {
                'css': cssclass,
                'tit': title} )
            entries.append('    <p class="%(css)s">%(txt)s</p>' % {
                'css': cssclass,
                'txt': msgdict[key]['text'].replace('\n', '<br/>')})
            entries.append('</details>')

    return entries


def WriteHTML(entries, title='Radio Broadcasts', refresh=3600, outfile='RadioWeather.html'):
    """
    Write out an html file from the preformatted list of entries.

    @param entries: entries ot write, already formatted
    @param title: title statement for the html
    @param refresh: automatic refresh time in seconds; 0 disables auto-refresh
    @param outfile: html file created
    """
    logger.info('Creating html output to %s', outfile)
    f = open(outfile, 'w')
    f.write('<!DOCTYPE html>\n')
    f.write('<html>\n')
    f.write('<head>\n')
    f.write('<meta charset="UTF-8"/>\n')
    if refresh != 0:
        f.write('<meta http-equiv="refresh" content="%s">\n' % str(refresh))
    f.write('<meta name="viewport" content="width=device-width, initial-scale=1.0" />\n')
    f.write('<link href="dwdrtty.css" rel="stylesheet" type="text/css">\n')
    f.write('<title>Sea Weather</title>\n')
    f.write('</head>\n')
    f.write('<body>\n\n')
    f.write('<h1>%s</h1>\n' % title)

    f.write('\n'.join(entries))

    f.write('</body>')
    f.close()

